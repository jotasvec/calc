import React, { Component } from 'react';
import  { evaluate  } from 'mathjs';
import { type } from 'os';

class CalculatorBox extends Component{
    state = {
        numbers_calc: ["1","2","3","4","5","6","7","8","9","0","."],
        operators_calc: ["+","-","*","/","^","sqrt"],
        parenthesis: ["(",")"],
        res: '0'
    }

    onDigit = (digit) => {
        // si el primer digito es un 0 se reemplaza por el que se apreta, casi contrario se agraga a la cadena de numeros
        const nDigit = this.state.res === '0' ? digit : this.state.res + digit
        console.log(nDigit)
        this.setState({
            res: nDigit
        })
    }


    execOperator = () => {
        try {
            const result = evaluate(this.state.res)
            this.setState({res: result.toString()})
        } catch (error) {
            this.setState({res: error})            
        }
    }

    // addNumber = (e) => {
    //     this.setState({
    //         res: 1
    //     })
    // }

    
    onChange = (e) => this.setState({
        [e.target.number]:e.target.value
    })
    
    render(){
        return(
            <div className="container row" >
                <div className="visor row">
                    <div className="input-field col s4">
                        <input type="text" name="visor_lbl"  onChange={this.onChange} value={this.state.res} />
                        {/* <label>Calculadora</label> */}
                    </div>
                </div>
                <div className="box-panel row" >
                    {/* <a className="waves-effect waves-light btn col s1" onClick={this.onDigit('1')}>1</a> */}
                    <div className="numbers">
                        {
                            this.state.numbers_calc.map(digit => {
                                return (                                   
                                        <a className="waves-effect waves-light btn col s3"
                                            onClick={() => this.onDigit(digit)} > {digit} </a>
                                );
                            })
                        }
                        <a className="waves-effect waves-light btn col s3 btn-equal" 
                                        onClick={this.execOperator}> = </a>
                    </div>
                    <div className="operator-box">
                        <div className="operator-btns">
                            {/* maths operators */}
                            {
                                this.state.operators_calc.map(operator => {
                                    return (
                                        <a className="waves-effect waves-light btn col s3" 
                                            
                                            onClick={() => 
                                            this.onDigit(this.state.operators_calc.includes(this.state.res.charAt(this.state.res.length-1)) ? '':operator)}>{operator}
                                        </a>
                                    );
                                })
                            }

                            {/* parenthesis */}
                            {
                                this.state.parenthesis.map(parenthesis => {
                                    return (
                                        <a className="waves-effect waves-light btn col s3"
                                            
                                            onClick={() => 
                                            this.onDigit(parenthesis)}>{parenthesis}
                                        </a>
                                    )
                                })
                            }
                        </div> {/* end operator-btns */}
                        <div className="clear-btns">
                            {/* btn C "clear" */}
                            <a className="waves-effect waves-light btn col s3" 
                                            onClick={() => 
                                            this.setState({res:'0'})}>C
                                        </a>
                            {/* btn del "delete de last one " */}
                            <a className="waves-effect waves-light btn col s3" 
                                            onClick={() => 
                                            this.setState({res:this.state.res.slice(0,this.state.res.length-1)})}> del
                                        </a>                        
                        </div>

                    </div>


                </div>
                
                <style jsx>
                    {`
                        .box-panel{
                            display: flex;
                            align-content: space-around;    
                        }
                        .numbers{
                            margin: 0 1rem;
                        }
                        .numbers a {
                            margin: 0.5rem;
                        }
                        .btn-equal{
                            background: green;
                        }
                        .operator-box{
                            display: grid;
                            align-content: space-around;
                        }
                        .operator-box a{
                            background: orange;
                            margin: 0.5rem;
                        }
                        .clear-btns a{
                            background: red;
                        }

                        
                    `}
                </style>


            </div>
        )
    }
    
}
// const CalcStyles = {
//     boxPanel:{
//         display: 'flex',
//         alignContent: 'space-around',
//     },

//     btnNumbers:{
//         margin: '0.5rem'
//     },
//     operatorBox:{
//         alignContent: 'space-beetwen',

//     },
//     operatorBtns:{
//         alignContent: 'space-around',

//     },

//     btnOprators:{
//         margin: '0.5rem',
//         backgroundColor: 'orange',
//     }, 
//     btnClears:{
//         margin: '0.5rem',
//         backgroundColor: 'red',
//     }
// }

export default CalculatorBox;