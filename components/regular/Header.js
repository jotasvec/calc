import Link from "next/link"


const links = [
    {url:"/", link_title:"Inicio"},
    {url:"/calculator", link_title:"Calculadora"}

]


export default () => (
    <div className="navbar-fixed">
        <nav>
        <div className="nav-wrapper">
            <a href="/" className="brand-logo">React and Next</a>
            <ul className="right hide-on-med-and-down">
                {
                    links.map(link=>{
                        return (
                            <li key={link.url}>
                                {/* aqui se hace un mapeo de los link se genera un tipo the dor con el links.map que ira recorrieno 
                                el array links con los datos correspondientes, por norma se necesita un key en el li  */}
                                <Link href={link.url}><a>{link.link_title}</a></Link>
                            </li>

                        )
                    })
                }
            </ul>
        </div>
        </nav>
    </div>
)