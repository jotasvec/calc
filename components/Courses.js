
export default (props) => (
    <div id="course-container">
        <h2>Curso de {props.course_name}</h2>
        {props.course.map(item =>{
            return(
                <div className="course-list">
                    <img src={item.image} alt="profile picture"/>
                    <p>{item.first_name} {item.last_name}</p>
                </div>
            )
        })}
        <style jsx>
            {
                `
                    #course-container{
                        perspective: 500px;
                    }
                    .course-list{
                        display: flex;
                        margin-bottom: 10px;
                    }
                    .course-list img{
                        padding: 5px;
                        border: solid 1px rgba(0,0,0,0.2);
                        border-radius: 50%;
                    }
                    .course-list p{
                        margin-left: 10px;
                        align-self: center;

                    }

                `
            }
        </style>
    </div>
)