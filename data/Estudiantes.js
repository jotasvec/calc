
// estudiantes a exportar
const matematica = [{
    "id": 1,
    "first_name": "Levi",
    "last_name": "Jepson",
    "email": "ljepson0@1688.com",
    "image": "https://robohash.org/quialiasducimus.jpg?size=50x50&set=set1"
  }, {
    "id": 2,
    "first_name": "Charlean",
    "last_name": "Bockh",
    "email": "cbockh1@multiply.com",
    "image": "https://robohash.org/nullaetenim.jpg?size=50x50&set=set1"
  }, {
    "id": 3,
    "first_name": "Mariya",
    "last_name": "Schimek",
    "email": "mschimek2@google.com.hk",
    "image": "https://robohash.org/earumharumaliquid.bmp?size=50x50&set=set1"
  }, {
    "id": 4,
    "first_name": "Danny",
    "last_name": "Lukes",
    "email": "dlukes3@msn.com",
    "image": "https://robohash.org/officiisducimusnulla.jpg?size=50x50&set=set1"
  }, {
    "id": 5,
    "first_name": "Maryanna",
    "last_name": "Wigg",
    "email": "mwigg4@ustream.tv",
    "image": "https://robohash.org/utdoloreerror.bmp?size=50x50&set=set1"
  }, {
    "id": 6,
    "first_name": "Lynea",
    "last_name": "Lenthall",
    "email": "llenthall5@geocities.jp",
    "image": "https://robohash.org/enimsintmagnam.png?size=50x50&set=set1"
  }, {
    "id": 7,
    "first_name": "Merla",
    "last_name": "Mocher",
    "email": "mmocher6@opensource.org",
    "image": "https://robohash.org/minimaautaliquid.jpg?size=50x50&set=set1"
  }, {
    "id": 8,
    "first_name": "Thaddeus",
    "last_name": "Simmings",
    "email": "tsimmings7@ebay.com",
    "image": "https://robohash.org/ametquasarchitecto.jpg?size=50x50&set=set1"
  }, {
    "id": 9,
    "first_name": "Levon",
    "last_name": "Focke",
    "email": "lfocke8@opera.com",
    "image": "https://robohash.org/autpariaturnihil.png?size=50x50&set=set1"
  }, {
    "id": 10,
    "first_name": "Maison",
    "last_name": "Brearley",
    "email": "mbrearley9@addthis.com",
    "image": "https://robohash.org/rationecumiusto.png?size=50x50&set=set1"
}]
const ciencia = [{
"id": 1,
"first_name": "Marya",
"last_name": "Allix",
"email": "mallix0@over-blog.com",
"image": "https://robohash.org/hicnullaquia.png?size=50x50&set=set1"
}, {
"id": 2,
"first_name": "Corrinne",
"last_name": "Softley",
"email": "csoftley1@networkadvertising.org",
"image": "https://robohash.org/eteumpariatur.jpg?size=50x50&set=set1"
}, {
"id": 3,
"first_name": "Ursala",
"last_name": "Reinert",
"email": "ureinert2@yolasite.com",
"image": "https://robohash.org/accusamusveniamlabore.jpg?size=50x50&set=set1"
}, {
"id": 4,
"first_name": "Milo",
"last_name": "Karel",
"email": "mkarel3@pinterest.com",
"image": "https://robohash.org/voluptatesquivitae.png?size=50x50&set=set1"
}, {
"id": 5,
"first_name": "Deidre",
"last_name": "Caplan",
"email": "dcaplan4@dailymotion.com",
"image": "https://robohash.org/autetdolor.bmp?size=50x50&set=set1"
}, {
"id": 6,
"first_name": "Burk",
"last_name": "Ivashnikov",
"email": "bivashnikov5@tiny.cc",
"image": "https://robohash.org/similiqueplaceatdolorum.jpg?size=50x50&set=set1"
}, {
"id": 7,
"first_name": "Cale",
"last_name": "Niland",
"email": "cniland6@cbslocal.com",
"image": "https://robohash.org/voluptatemmolestiaeaut.bmp?size=50x50&set=set1"
}, {
"id": 8,
"first_name": "Emmie",
"last_name": "Eldred",
"email": "eeldred7@networkadvertising.org",
"image": "https://robohash.org/etomniset.png?size=50x50&set=set1"
}, {
"id": 9,
"first_name": "Norry",
"last_name": "Summersby",
"email": "nsummersby8@chronoengine.com",
"image": "https://robohash.org/eosdebitisnobis.png?size=50x50&set=set1"
}, {
"id": 10,
"first_name": "Nada",
"last_name": "Shirtcliffe",
"email": "nshirtcliffe9@prnewswire.com",
"image": "https://robohash.org/voluptasdeseruntprovident.jpg?size=50x50&set=set1"
}]


// usando es6 se puede exportar asi
module.exports = {
    matematica, ciencia
}