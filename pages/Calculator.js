import Head from "../components/regular/Head";
import Header from "../components/regular/Header"
import CalculatorBox from "../components/CalculatorBox"

export default () => (
    <div>
        <Head/>
        <Header/>
        <h1>Calculadora</h1>
        <CalculatorBox style={{textAlign:"center"}} />
    </div>
)